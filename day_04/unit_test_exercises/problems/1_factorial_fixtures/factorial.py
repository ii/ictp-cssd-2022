class SomeError(Exception):
    pass

def factorial(number):
    if number<0:
        raise SomeError("test")
    if number ==0:
        return 1
    if number <=1:
        return number 
    return factorial(number-1)*number
