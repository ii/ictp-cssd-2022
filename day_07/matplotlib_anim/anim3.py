import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

fig, ax = plt.subplots()
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)

# format is xpos, ypos, xspeed, yspeed
dots = np.random.rand(30,4)

# velocity should be positive and negative
dots[:, 2:] -= 0.5
# velocity much smaller than position
dots[:, 2:] *= 0.01

position = dots[:,:2]
speed    = dots[:,2:]

xdata = dots[:,0]
ydata = dots[:,1]
xspeed = dots[:,2]
yspeed = dots[:,3]

graph, = plt.plot([], [], 'bo')

def update(frame):

    xdata[:] += xspeed
    ydata[:] += yspeed

    below_floor = ydata < 0
    yspeed[below_floor] *= -1
    ydata[below_floor] = 0

    graph.set_data(xdata, ydata)
    return graph,

ani = FuncAnimation(
    fig,
    update,
    # frames=250,
    interval=30,
    repeat=False,
)

# with open('dots4.html','w') as f:
#    f.write(ani.to_html5_video())

# ani.save('dots.mp4')

plt.show()
