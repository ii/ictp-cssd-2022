import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

fig, ax = plt.subplots()
ax.set_xlim(0, 7)
ax.set_ylim(-1, 1)

xdata, ydata = [], []
line, = plt.plot([], [], 'ro')

def update(frame):
    xdata.append(frame)
    ydata.append(np.sin(frame))
    line.set_data(xdata, ydata)
    return line,

ani = FuncAnimation(
    fig,
    update,
    frames=np.linspace(0, 2*np.pi, 128),
    interval=25,
    repeat=False,
)

#with open('sine.html','w') as f:
#    f.write(ani.to_html5_video())

#ani.save('sine.mp4')

plt.show()
