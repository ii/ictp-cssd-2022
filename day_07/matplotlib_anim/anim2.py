import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

fig, ax = plt.subplots()
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)

# format is xpos, ypos, xspeed, yspeed
dots = np.array([
    [0.3, 0.7, 0.02, -0.01],
    [0.7, 0.4, -0.02, 0.03],
    [0.1, 0.1, 0.02, 0.02],
])

#position = dots[:,:2]
#speed    = dots[:,2:]
xdata = dots[:,0]
ydata = dots[:,1]
xspeed = dots[:,2]
yspeed = dots[:,3]

graph, = plt.plot([], [], 'bo')

def update(frame):
    #position[:] += speed
    xdata[:] += xspeed
    ydata[:] += yspeed
    graph.set_data(xdata, ydata)
    return graph,

ani = FuncAnimation(
    fig,
    update,
    frames=40,
    interval=50,
    repeat=False,
)

# with open('dots.html','w') as f:
#     f.write(ani.to_html5_video())

# ani.save('dots.mp4')

plt.show()
