import altair as alt
import pandas as pd
from altair import datum

# Read in and prep data
source = pd.read_json("basic_charts_data/penguins.json")
print(source.head())


parallel_penguins = (
    alt.Chart(source)
    .transform_window(index="count()")
    .transform_fold(
        [
            "Beak Length (mm)",
            "Beak Depth (mm)",
            "Flipper Length (mm)",
            "Body Mass (g)",
        ]
    )
    .transform_joinaggregate(min="min(value)", max="max(value)", groupby=["key"])
    .transform_calculate(
        minmax_value=(datum.value - datum.min) / (datum.max - datum.min),
        mid=(datum.min + datum.max) / 2,
    )
    .mark_line()
    .encode(
        x="key:N",
        y="value:Q",
        color="Species:N",
        detail="index:N",
        opacity=alt.value(0.5),
    )
    .properties(height=500, width=1000)
)
parallel_penguins.show()
