2022 ICTP Collaborative programming school
==========================================

In this repository, we'll include all relevant material, it will be updated frequently during the school.

Discord
-------

We use Discord for sharing short links etc. during the sessions, and for coordinating 
the group work in the second week. You can also use it to chat with the other participants.

https://discord.gg/zEbvcdme

Timetable
---------

This is the current day-by-day plan, but we may switch up some items at short notice.

----

Day 1 - Monday
..............

* 10:30 - Welcome and introduction. 
* 14:00 - `Short recap of Python <day_01/2022-Py-intro.pdf>`_, Exercises: https://projecteuler.net Task 1,2 and 14
* 16:00 - `Intro to git <day_01/2022-Git-introt.pdf>`_



Day 2 - Tuesday
...............

* 09:30 - `Clean code & documentation <day_02/2022-clean-code.pdf>`_ 
* 11:15 - Computer architectures, memory layout
* 14:00 - `Numpy overview <day_02/01-ICTP-Py-3-numpy.pdf>`_
* 14:30 - `Numpy activity <https://ictp.grelli.org/sd2021/landcover/>`_


Day 3 - Wednesday
.................

* 09:30 - Git branching and merging
* 11:20 - `Object oriented design <day_03/01-OO-Design.pdf>`_
* 14:00 - `Train station exercise <day_03/trains/>`_


Day 4 - Thursday
................

see day_04



Day 5 - Friday
.............. 

see day_05

----

   Changed sequence of lecture topics in week 2!

----

Day 6 - Monday
..............

* 09:30 - Gitlab pull, push, multiple authors, conversational development
* 11:15 - Documentation & tools
* 12:15 - choice of group project
* 14:00 - Continuous Integration
* afterwards - work on project setup

The git repo for the student projects is at https://git.smr3696.ictp.it/ 

Project summaries:
* A: `Crowds <day_06/crowdphysics_2022.pdf>`_
* B: Proteins
* C: `Planets <https://uib.grelli.org/cssd2022/planets/index.html>`_
* D: `Bicycles <https://uib.grelli.org/cssd2022/bikegenes/index.html>`_

Day 7 - Tuesday
...............

* 09:30 - Virtualenv & Docker
* 11:15 - Animations
* 12:15 - Floating point is scary
* afterwards - work on project


Day 8 - Wednesday
.................
 
* 09:30 - Using Makefiles for more than just compiling
* 11:15 - A showcase of the lecturers' own projects
* afterwards - work on project

Day 9 - Thursday
................
   
* 09:30 - Copyright and Licensing
* afterwards - work on project

Day 10 - Friday
...............

* 09:30 - Prepare presentations (10 min each)
* 10:30 - coffee break
* 11:00 - Presentations
* 12:15 - Final remarks, course certificates
* 12:30 - close 

